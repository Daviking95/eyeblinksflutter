import 'package:flutter/cupertino.dart';

class FaceModel {
  double? smile;
  double? rightYearsOpen;
  double? leftYearsOpen;
  double? headEulerAngleY;
  double? headEulerAngleZ;

  FaceModel({
    this.smile,
    this.rightYearsOpen,
    this.leftYearsOpen,
    this.headEulerAngleY,
    this.headEulerAngleZ
  });
}
