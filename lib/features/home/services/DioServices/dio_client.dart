import 'dart:async';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../http_services.dart';
import 'network_exceptions.dart';

const _defaultConnectTimeout = Duration.millisecondsPerMinute;
const _defaultReceiveTimeout = Duration.millisecondsPerMinute;

class DioClient {
  String? baseUrl = NFaceService.baseUrl;

  Dio _dio = new Dio();

  String? token;

  final List<Interceptor>? interceptors;

  var timeLimit = const Duration(seconds: 1);

  DioClient({this.interceptors, this.token, this.baseUrl}) {
    _dio
      ..options.baseUrl = baseUrl ?? NFaceService.baseUrl
      ..options.connectTimeout = 120000 // 1 minutes // _defaultConnectTimeout
      ..options.receiveTimeout = _defaultReceiveTimeout
      ..httpClientAdapter
      ..options.headers = {'Content-Type': 'application/json; charset=UTF-8'};
    // ..interceptors.add(RetryOnConnectionChangeInterceptor(
    //   requestRetrier: DioConnectivityRequestRetrier(
    //     dio: _dio,
    //     connectivity: Connectivity(),
    //   ),
    // ));
    if (token != null && token!.isNotEmpty) {
      _dio.options.headers = {'Authorization': 'Bearer $token'};
    }
    if (interceptors?.isNotEmpty ?? false) {
      _dio.interceptors.addAll(interceptors!);
    }
    if (kDebugMode) {
      _dio.interceptors.add(LogInterceptor(
          responseBody: true,
          error: true,
          requestHeader: false,
          responseHeader: false,
          request: false,
          requestBody: false));
    }
  }

  Future<dynamic> get(
    String uri, {
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    ProgressCallback? onReceiveProgress,
  }) async {
    try {
      var response = await _dio.get(
        uri,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onReceiveProgress: onReceiveProgress,
      );

      NetworkExceptions.handleResponse(response.statusCode);

      return response.data;
    } on DioError catch (e) {
      throw (DioError(response: e.response, requestOptions: e.requestOptions));
    } on TimeoutException catch (e) {
      throw TimeoutException(e.toString());
    } on SocketException catch (e) {
      throw SocketException(e.toString());
    } on HttpException catch (e) {
      throw HttpException(e.toString());
    } on FormatException catch (_) {
      throw FormatException();
      // throw FormatException("Unable to process the data");
    } catch (e) {
      throw e;
    }
  }

  Future<dynamic> post(
    String uri, {
    data,
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    try {
      var response = await _dio.post(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );

      NetworkExceptions.handleResponse(response.statusCode);

      return response.data;
    } on DioError catch (e) {
      throw (DioError(response: e.response, requestOptions: e.requestOptions));
    } on TimeoutException catch (e) {
      throw TimeoutException(e.toString());
    } on SocketException catch (e) {
      throw SocketException(e.toString());
    } on HttpException catch (e) {
      throw HttpException(e.toString());
    } on FormatException catch (_) {
      throw FormatException();
    } catch (e) {
      throw e;
    }
  }

  Future<dynamic> patch(
    String uri, {
    data,
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    try {
      var response = await _dio.patch(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );

      NetworkExceptions.handleResponse(response.statusCode);

      return response.data;
    } on DioError catch (e) {
      throw (DioError(response: e.response, requestOptions: e.requestOptions));
    } on TimeoutException catch (e) {
      throw TimeoutException(e.toString());
    } on SocketException catch (e) {
      throw SocketException(e.toString());
    } on HttpException catch (e) {
      throw HttpException(e.toString());
    } on FormatException catch (_) {
      throw FormatException();
    } catch (e) {
      throw e;
    }
  }

  Future<dynamic> delete(
    String uri, {
    data,
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
  }) async {
    try {
      var response = await _dio.delete(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
      );

      NetworkExceptions.handleResponse(response.statusCode);

      return response.data;
    } on DioError catch (e) {
      throw (DioError(response: e.response, requestOptions: e.requestOptions));
    } on TimeoutException catch (e) {
      throw TimeoutException(e.toString());
    } on SocketException catch (e) {
      throw SocketException(e.toString());
    } on HttpException catch (e) {
      throw HttpException(e.toString());
    } on FormatException catch (_) {
      throw FormatException();
    } catch (e) {
      throw e;
    }
  }
}

// dio.post().then(){ }.catchError(){ }

/*
*     (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
*
* */
