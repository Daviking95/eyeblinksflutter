import 'dart:convert';
import 'dart:developer';

import 'package:google_ml_example/features/home/model/face_auth_success_response_model.dart';
import 'package:google_ml_example/features/home/model/fetch_login_success_response_model.dart';

import 'DioServices/dio_client.dart';

class NFaceService {
  static String baseUrl = "https://pass.sterling.ng/nface/api/Nibss/";

  Future<FetchLoginSuccessResponseModel> fetchLoginService() async {
    // TODO: implement getActiveServiceConfigService
    try {
      final Map<String, dynamic> _requestData = {
        "username": "pekla",
        "password": "E,,<9,'&)9&@R[uk9agDNQ`hQ+Gt6U2+",
        "bvn": "22277688363",
      };

      print("$baseUrl/FetchLogin");
      log("$baseUrl/FetchLogin");

      var responseData = await DioClient(baseUrl: baseUrl)
          .post("FetchLogin", data: _requestData);

      print("responseData ${responseData}");
      log("responseData ${responseData}");

      return FetchLoginSuccessResponseModel.fromJson(responseData);
    } catch (e) {
      return FetchLoginSuccessResponseModel(
        responseCode: "01",
        responseMessage: "Could not reach endpoint. Please try again later.",
        token: null,
      );
    }
  }

  Future<FaceAuthSuccessResponseModel> faceAuthenticationService(
      String token, String image) async {
    // TODO: implement getActiveServiceConfigService
    try {
      final Map<String, dynamic> _requestData = {
        "username": "pekla",
        "password": "E,,<9,'&)9&@R[uk9agDNQ`hQ+Gt6U2+",
        "bvn": "22277688363",
        "token": token,
        "image": image,
        "device": "MOBILE",
      };

      print("$baseUrl/face-authentication");
      log("$baseUrl/face-authentication");

      log("_requestData ${jsonEncode(_requestData)}");

      var responseData = await DioClient(baseUrl: baseUrl)
          .post("face-authentication", data: _requestData);

      print("responseData ${responseData}");
      log("responseData ${responseData}");

      return FaceAuthSuccessResponseModel.fromJson(responseData);
    } catch (e) {
      return FaceAuthSuccessResponseModel(
          responseCode: '01', message: "Error occurred");
    }
  }
}
