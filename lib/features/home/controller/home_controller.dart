import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:google_ml_example/features/home/controller/camera_controller.dart';
import 'package:google_ml_example/features/home/controller/face_detection_controller.dart';
import 'package:google_ml_example/features/home/model/face_auth_success_response_model.dart';
import 'package:google_ml_example/features/home/model/fetch_login_success_response_model.dart';
import 'package:google_ml_example/features/home/module/face_model.dart';
import 'package:google_ml_example/features/home/services/http_services.dart';
import 'package:google_ml_example/features/home/view/display_screen.dart';
import 'package:google_ml_kit/google_ml_kit.dart';

class HomeController extends GetxController {
  CameraManager? _cameraManager;
  CameraController? cameraController;
  FaceDetetorController? _faceDetect;
  bool _isDetecting = false;
  List<FaceModel>? faces;
  String? faceAtMoment = 'normal_face.png';
  String? label = 'Normal';
  static GlobalKey previewContainer = new GlobalKey();

  bool _isLoading = false.obs();

  bool get isLoading => _isLoading;

  set isLoading(bool value) {
    _isLoading = value;

    update();
  }

  HomeController() {
    _cameraManager = CameraManager();
    _faceDetect = FaceDetetorController();
  }

  Future<void> loadCamera() async {
    cameraController = await _cameraManager?.load();
    update();
  }

  Future<void> startImageStream() async {
    CameraDescription camera = cameraController!.description;

    cameraController?.startImageStream((cameraImage) async {
      if (_isDetecting) return;

      _isDetecting = true;

      final WriteBuffer allBytes = WriteBuffer();
      for (Plane plane in cameraImage.planes) {
        allBytes.putUint8List(plane.bytes);
      }
      final bytes = allBytes.done().buffer.asUint8List();

      final Size imageSize =
          Size(cameraImage.width.toDouble(), cameraImage.height.toDouble());

      final InputImageRotation imageRotation =
          InputImageRotationMethods.fromRawValue(camera.sensorOrientation) ??
              InputImageRotation.Rotation_0deg;

      final InputImageFormat inputImageFormat =
          InputImageFormatMethods.fromRawValue(cameraImage.format.raw) ??
              InputImageFormat.NV21;

      final planeData = cameraImage.planes.map(
        (Plane plane) {
          return InputImagePlaneMetadata(
            bytesPerRow: plane.bytesPerRow,
            height: plane.height,
            width: plane.width,
          );
        },
      ).toList();

      final inputImageData = InputImageData(
        size: imageSize,
        imageRotation: imageRotation,
        inputImageFormat: inputImageFormat,
        planeData: planeData,
      );

      InputImage inputImage = InputImage.fromBytes(
        bytes: bytes,
        inputImageData: inputImageData,
      );

      processImage(inputImage, cameraController!);
    });
  }

  Future<void> processImage(
    InputImage inputImage,
    CameraController cameraController,
  ) async {
    faces = await _faceDetect?.processImage(inputImage);

    print("faces $faces");

    if (faces != null && faces!.isNotEmpty) {
      FaceModel? face = faces?.first;
      label = detectSmile(face!, cameraController);
    } else {
      faceAtMoment = 'normal_face.png';
      label = 'No face detected';
    }
    _isDetecting = false;
    update();
  }

  String detectSmile(
    FaceModel face,
    CameraController cameraController,
  ) {
    if (face.smile! > 0.86) {
      faceAtMoment = 'happy_face.png';
      return 'Big smile with teeth';
    } else if (face.smile! > 0.8) {
      faceAtMoment = 'happy_face.png';
      return 'Big Smile';
    } else if (face.smile! > 0.3) {
      faceAtMoment = 'happy_face.png';
      return 'Smile';
    } else if (face.rightYearsOpen! < 0.4 || face.leftYearsOpen! < 0.4) {
      faceAtMoment = 'ok.png';

      _takePicture(cameraController).then((res) async {
        // final bytes = File(res!).readAsBytesSync().;

        Navigator.of(Get.context!).push(
          MaterialPageRoute(
            builder: (context) => DisplayPictureScreen(
              file: res,
            ),
          ),
          // (_) => false
        );
      });

      // List<int> imageBytes = File(inputImage.filePath!).readAsBytesSync();
      // print(imageBytes);
      // String base64Image = base64Encode(imageBytes);
      //
      // print("base64Image $base64Image");

      return 'Eyes blinked';
    }
    // else if (face.headEulerAngleY! < 35) {
    //   faceAtMoment = 'happy_face.png';
    //   return 'Detect Right movement';
    // } else if (face.headEulerAngleY! < -35) {
    //   faceAtMoment = 'happy_face.png';
    //   return 'Detect Left movement';
    // }
    else {
      faceAtMoment = 'sady_face.png';
      return 'Sad';
    }
  }

  String uint8ListTob64(Uint8List uint8list) {
    String base64String = base64Encode(uint8list);
    String header = "data:image/png;base64,";
    return header + base64String;
  }

  Future<XFile> _takePicture(CameraController cameraController) async {
    // try {
    late XFile base64String;
    if (cameraController.value.isInitialized) {
      // cameraController.startVideoRecording();
      // _cameraManager!.controller!.buildPreview();

      await cameraController.initialize();

      base64String = await cameraController.takePicture();
      print("The Image ${base64String}");
      print("CameraPreview }");
    }
    return base64String;

    // } catch (e) {
    //   print(e.toString());
    // }
  }

  runOperations(XFile file, BuildContext context) async {
    NFaceService _nFaceService = new NFaceService();

    _isLoading = true;
    isLoading = true;
    update();

    try {
      FetchLoginSuccessResponseModel fetchLoginSuccessResponseModel =
          await _nFaceService.fetchLoginService();

      if (fetchLoginSuccessResponseModel.responseCode != '01') {
        // print("runOperations $file");
        // log("runOperations $file");

        String base64Image = base64Encode(File(file.path).readAsBytesSync());

        // print("base64Image $base64Image");
        // log("base64Image $base64Image");

        FaceAuthSuccessResponseModel faceAuthSuccessResponseModel =
            await _nFaceService.faceAuthenticationService(
                fetchLoginSuccessResponseModel.token!, base64Image);

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(faceAuthSuccessResponseModel.message ?? ""),
          backgroundColor: faceAuthSuccessResponseModel.status == 1
              ? Colors.green
              : Colors.red,
          duration: Duration(seconds: 15),
          elevation: 2,
        ));

        _isLoading = false;
        isLoading = false;
        update();
      } else {
        _isLoading = false;
        isLoading = false;
        update();

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(fetchLoginSuccessResponseModel.responseMessage ??
              "Error occurred. Please try again"),
        ));
      }
    } catch (e) {
      _isLoading = false;
      isLoading = false;
      update();

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Could not proceed from here. Please try again"),
      ));
    }
  }
}
