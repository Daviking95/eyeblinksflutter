// A widget that displays the picture taken by the user.
import 'dart:developer';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_ml_example/features/home/controller/home_controller.dart';
import 'package:google_ml_example/features/home/view/home_page.dart';

class DisplayPictureScreen extends StatefulWidget {
  final XFile file;

  DisplayPictureScreen({required this.file});

  @override
  State<DisplayPictureScreen> createState() => _DisplayPictureScreenState();
}

class _DisplayPictureScreenState extends State<DisplayPictureScreen> {
  final _homeController = HomeController();

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    _homeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Display the Picture'),
        automaticallyImplyLeading: false,
        leading: GestureDetector(
            onTap: () => Get.offAll(HomePage()),
            child: Icon(
              Icons.arrow_back_sharp,
              size: 30,
            )),
      ),
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.

      body: GetBuilder<HomeController>(
        init: _homeController,
        initState: (_) async {
          print("file.path.isNotEmpty ${widget.file.path.isNotEmpty}");
          log("file.path.isNotEmpty ${widget.file.path.isNotEmpty}");
          if (widget.file.path.isNotEmpty) {
            _homeController.runOperations(widget.file, context);
          }
        },
        builder: (controller) {
          return Column(
            children: [
              Image.file(File(widget.file.path)),
              SizedBox(
                height: 30,
              ),
              _homeController.isLoading
                  ? Center(
                      child: CircularProgressIndicator(
                      color: Colors.red,
                    ))
                  : Container()
            ],
          );
        },
      ),
    );
  }
}
