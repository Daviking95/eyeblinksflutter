/// responseCode : "00"
/// responseMessage : "Successfully fetched token"
/// token : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjotNTgyOTE4ODk0Mzg0NDY3ODI0MiwiZXhwIjoxNjUwNjM5NjQ2fQ.Q13ek1SXqHCGZxu6CJuPbRo91fQwoogrSlrlAJKK2v8"

class FetchLoginSuccessResponseModel {
  FetchLoginSuccessResponseModel({
    String? responseCode,
    String? responseMessage,
    String? token,
  }) {
    _responseCode = responseCode;
    _responseMessage = responseMessage;
    _token = token;
  }

  FetchLoginSuccessResponseModel.fromJson(dynamic json) {
    _responseCode = json['responseCode'];
    _responseMessage = json['responseMessage'];
    _token = json['token'];
  }
  String? _responseCode;
  String? _responseMessage;
  String? _token;

  String? get responseCode => _responseCode;
  String? get responseMessage => _responseMessage;
  String? get token => _token;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['responseCode'] = _responseCode;
    map['responseMessage'] = _responseMessage;
    map['token'] = _token;
    return map;
  }
}
