/// responseCode : "01"
/// responseMessage : "Could not reach endpoint. Please try again later."
/// token : null

class FetchLoginErrorResponseModel {
  FetchLoginErrorResponseModel({
    String? responseCode,
    String? responseMessage,
    dynamic token,
  }) {
    _responseCode = responseCode;
    _responseMessage = responseMessage;
    _token = token;
  }

  FetchLoginErrorResponseModel.fromJson(dynamic json) {
    _responseCode = json['responseCode'];
    _responseMessage = json['responseMessage'];
    _token = json['token'];
  }
  String? _responseCode;
  String? _responseMessage;
  dynamic _token;

  String? get responseCode => _responseCode;
  String? get responseMessage => _responseMessage;
  dynamic get token => _token;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['responseCode'] = _responseCode;
    map['responseMessage'] = _responseMessage;
    map['token'] = _token;
    return map;
  }
}
