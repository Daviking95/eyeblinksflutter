class ErrorResponseModel {
  bool? _isSuccess;
  String? _message;
  int? _status;
  String? _token;

  ErrorResponseModel(
      {bool isSuccess = false, required String message, int status = 200, String token = ""}) {
    this._isSuccess = isSuccess;
    this._message = message;
    this._status = status;
    this._token = token;
  }

  bool get isSuccess => _isSuccess!;
  set isSuccess(bool isSuccess) => _isSuccess = isSuccess;
  String get message => _message!;
  set message(String message) => _message = message;
  int get status => _status!;
  set status(int status) => _status = status;
  String get token => _token!;
  set token(String token) => _token = token;

  ErrorResponseModel.fromJson(Map<String, dynamic> json) {
    _isSuccess = json['isSuccess'];
    _message = json['message'];
    _status = json['status'];
    _token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['isSuccess'] = this._isSuccess;
    data['message'] = this._message;
    data['status'] = this._status;
    data['token'] = this._token;
    return data;
  }
}
