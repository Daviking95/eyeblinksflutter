/// type : "https://tools.ietf.org/html/rfc7231#section-6.5.1"
/// title : "One or more validation errors occurred."
/// status : 400
/// traceId : "00-e0aa29d75d77804d9d00af6efb8e3943-82bebffab966a344-00"
/// errors : {"$.image":["The JSON value could not be converted to System.Byte[]. Path: $.image | LineNumber: 0 | BytePositionInLine: 49."]}

class FaceAuthErrorResponseModel {
  FaceAuthErrorResponseModel({
    String? type,
    String? title,
    int? status,
    String? traceId,
    Errors? errors,
  }) {
    _type = type;
    _title = title;
    _status = status;
    _traceId = traceId;
    _errors = errors;
  }

  FaceAuthErrorResponseModel.fromJson(dynamic json) {
    _type = json['type'];
    _title = json['title'];
    _status = json['status'];
    _traceId = json['traceId'];
    _errors = json['errors'] != null ? Errors.fromJson(json['errors']) : null;
  }
  String? _type;
  String? _title;
  int? _status;
  String? _traceId;
  Errors? _errors;

  String? get type => _type;
  String? get title => _title;
  int? get status => _status;
  String? get traceId => _traceId;
  Errors? get errors => _errors;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['type'] = _type;
    map['title'] = _title;
    map['status'] = _status;
    map['traceId'] = _traceId;
    if (_errors != null) {
      map['errors'] = _errors?.toJson();
    }
    return map;
  }
}

/// $.image : ["The JSON value could not be converted to System.Byte[]. Path: $.image | LineNumber: 0 | BytePositionInLine: 49."]

class Errors {
  Errors({
    List<String>? image,
  }) {
    _image = image;
  }

  Errors.fromJson(dynamic json) {
    _image = json['image'] != null ? json['image'].cast<String>() : [];
  }
  List<String>? _image;

  List<String>? get image => _image;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['image'] = _image;
    return map;
  }
}
