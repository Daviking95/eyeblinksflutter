/// responseCode : "00"
/// responseDescription : "Successfully sent request to nibss"
/// status : 2
/// message : "Liveness Detection Failure"

class FaceAuthSuccessResponseModel {
  FaceAuthSuccessResponseModel({
    String? responseCode,
    String? responseDescription,
    int? status,
    String? message,
  }) {
    _responseCode = responseCode;
    _responseDescription = responseDescription;
    _status = status;
    _message = message;
  }

  FaceAuthSuccessResponseModel.fromJson(dynamic json) {
    _responseCode = json['responseCode'];
    _responseDescription = json['responseDescription'];
    _status = json['status'];
    _message = json['message'];
  }
  String? _responseCode;
  String? _responseDescription;
  int? _status;
  String? _message;

  String? get responseCode => _responseCode;
  String? get responseDescription => _responseDescription;
  int? get status => _status;
  String? get message => _message;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['responseCode'] = _responseCode;
    map['responseDescription'] = _responseDescription;
    map['status'] = _status;
    map['message'] = _message;
    return map;
  }
}
